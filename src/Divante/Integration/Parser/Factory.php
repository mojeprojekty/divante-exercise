<?php

namespace Divante\Integration\Parser;

use Divante\Integration\Exception\ParserNotFound;
use Divante\Integration\Parser;

/**
 * Class Factory
 *
 * @package Divante\Integration\Parser
 */
class Factory implements FactoryInterface
{
    public function getParser($type)
    {
        $parserClass = "Divante\Integration\Parser\\".ucfirst($type);

        if (!class_exists($parserClass)) {
            throw new ParserNotFound("There is no such parser as '{$type}'");
        }

        return new $parserClass;
    }
}
