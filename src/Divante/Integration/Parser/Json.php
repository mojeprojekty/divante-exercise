<?php

namespace Divante\Integration\Parser;

use Divante\Integration\Exception\ParserException;

class Json implements ParserInterface
{

    public static function getType()
    {
        return 'json';
    }

    public function parse($content)
    {
        $output = json_decode($content, true);

        if (!$output) {
            throw new ParserException();
        }

        return $output;
    }
}