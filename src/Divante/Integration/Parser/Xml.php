<?php

namespace Divante\Integration\Parser;

class Xml implements ParserInterface
{

    public static function getType()
    {
        return 'xml';
    }

    public function parse($content)
    {
        $obj = simplexml_load_string($content);
        $json = json_encode($obj);
        $result = json_decode($json, true);

        return $result;
    }
}