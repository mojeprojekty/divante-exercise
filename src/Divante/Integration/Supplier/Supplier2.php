<?php

/*
 * This file is part of the "Divante/Integration" package.
 *
 * (c) Divante Sp. z o. o.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Divante\Integration\Supplier;

/**
 * Class Supplier2
 *
 * @package Divante\Integration\Supplier
 */
class Supplier2 extends SupplierAbstract
{
    /**
     * {@inheritdoc}
     */
    public static function getName()
    {
        return 'supplier2';
    }

    /**
     * {@inheritdoc}
     */
    public static function getResponseType()
    {
        return 'xml';
    }

    /**
     * {@inheritdoc}
     */
    protected function parseResponse()
    {
        $response = $this->parser->parse($this->getResponse());
        return $response['item'];
    }

    /**
     * Simulate get response method
     *
     * @return string
     */
    protected function getResponse()
    {
        return file_get_contents('http://divante-exercise.mojeprojekty.org/supplier2.xml');
    }
}
